import { MainExpenseModuleModule } from './app/main-expense-module/main-expense-module.module';
import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { environment } from './environments/environment';



if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(MainExpenseModuleModule)
  .catch(err => console.error(err));
