import { Category } from './category';
import { Contributor } from './contributor';

export class Expense {
  constructor(
    public id: number,
    public expName: string,
    public category: Category,
    public amount: number,
    public date: Date,
    public contributor: Contributor,
    public comments?: string
  )

  {  }
}
