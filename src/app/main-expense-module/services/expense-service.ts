import { ContributorService } from './contributor-service';
import { Category } from './../models/category';
import { Expense } from './../models/expense';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl,  Validators } from '@angular/forms';
import { Contributor } from '../models/contributor';
import { MatTableDataSource } from '@angular/material/table';
import { Observable } from 'rxjs';




const getAllCategoriesEndPoint = 'http://localhost:8090/getAllCatogeries';
const createExpenseEndPoint = 'http://localhost:8090/createExpense';
const getAllExpensesEndPoint = 'http://localhost:8090/getAllExpenses';
const deleteExpensesEndPoint = 'http://localhost:8090/deleteExpense';


@Injectable({
  providedIn: 'root'
})
export class ExpenseService {




  total: number;
  expenseList: Expense[] = [];
  expenseListNew: Expense[] = [];
  categoriesList: Category[] = [];
  contributorList: Contributor[] = [];


  constructor(private http: HttpClient , private contributorService: ContributorService) {

    this.getAllCategories();
    this.getAllContributors();

  }

  monthlyReportForm = new FormGroup(
    {
      fromDate: new FormControl(''),
      toDate: new FormControl('')
    }
  );

  createExpenseForm = new FormGroup({

    expName: new FormControl('', [Validators.required, Validators.email]),
    comments: new FormControl('', [Validators.required, Validators.minLength(3)]),
    date: new FormControl('', Validators.required),
    amount: new FormControl('', Validators.required),
    category: new FormControl(''),
    contributor: new FormControl('')
  });

  getAllExpensesForMonth(fromDate: any, toDate: any,  expenseList: Expense[], total: number) {

    total = 0;
    this.http.get(getAllExpensesEndPoint.concat('/' + fromDate + '/' + toDate)).subscribe(
      result => {
        for (const d of (result as Array<Expense>))
        {
            total = total + d.amount;
            console.log(total);
            expenseList.push(d);
        }
    }
    );
    return total;
  }

  createExpense(value: any) {
    console.log('Create Expense');
    this.http.post(createExpenseEndPoint, value).subscribe();
  }



  populateCreateExpenseFormData(data: Expense) {

    //this.createExpenseForm.setValue(data);
    //this.categoriesList.push(data.category);

    console.log('pre populateting form data::'+ data.category.id);
    this.createExpenseForm = new FormGroup({
      id: new FormControl(data.id),
      expName: new FormControl(data.expName, [Validators.required, Validators.email]),
      comments: new FormControl(data.comments, [Validators.required, Validators.minLength(3)]),
      date: new FormControl(data.date, Validators.required),
      amount: new FormControl(data.amount, Validators.required),
      category: new FormControl(data.category.id),
      contributor: new FormControl(data.contributor.id)
    });

  }

  getAllCategories() {
     console.log('hii');
     return this.http.get(getAllCategoriesEndPoint).subscribe(
      result => {
          for (const d of (result as Array<Category>))
          {
              this.categoriesList.push(d);
          }
      }

    );
  }


  getAllContributors()
  {
    this.contributorService.getAllCcontributors().subscribe(
      result => {
          for (const d of (result as Array<Contributor>))
          {
              this.contributorList.push(d);
          }
      }

    );

  }

  deleteExpense(expense: Expense) {
    this.http.delete(deleteExpensesEndPoint + '/' + expense.id).subscribe();
  }

  getAllExpenses() {

   return this.http.get(getAllExpensesEndPoint);

}

getAllExpensesNew(): Observable<Expense[]>{
  return this.http.get<Expense[]>(getAllExpensesEndPoint);
}


}
