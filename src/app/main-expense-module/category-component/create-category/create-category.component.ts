import { FormControl, FormsModule, FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import {CategoryService} from '../../services/category.service';

@Component({
  selector: 'app-create-category',
  templateUrl: './create-category.component.html',
  styleUrls: ['./create-category.component.styl']
})
export class CreateCategoryComponent implements OnInit {

  categoryForm: FormGroup;
  constructor(private categoryService: CategoryService) {

    this.categoryForm =  this.createCategoryFormGroup();
   }

  ngOnInit(): void {
  }

  onSubmit(): void {
    console.log(this.categoryForm.get('category').value);
    this.categoryService.createCategory(this.categoryForm.get('category').value);
  }

  createCategoryFormGroup(): FormGroup
    {
        return  new FormGroup({
          category : new FormControl(''),
    });
  }

}
