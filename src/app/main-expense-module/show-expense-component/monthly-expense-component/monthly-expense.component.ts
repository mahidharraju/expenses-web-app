import { CreateExpenseFormComponent } from './../../create-expense-component/create-expense-form.component';
import { DeleteExpenseComponent } from './../delete-expense/delete-expense.component';
import { ExpenseService } from './../../services/expense-service';
import { Expense } from './../../models/expense';
import { Component, OnInit, ViewChild , } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatTableDataSource} from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator, MatPaginatorModule } from '@angular/material/paginator';





@Component({
  selector: 'app-monthly-expense',
  templateUrl: './monthly-expense.component.html',
  styleUrls: ['./monthly-expense.component.styl']
})
export class MonthlyExpenseComponent implements OnInit {

  total: number;
  expenseList: any;
  expenseTableColumns: string[] = ['expName', 'description', 'amount' , 'category' , 'date' , 'contributor' , 'actions'];
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;



  constructor(public expenseService: ExpenseService , public matDialog: MatDialog) {

    }

  ngOnInit(): void {
    console.log('Init called');
    this.expenseList = new MatTableDataSource();

    this.getAllExpenses();

  }


  submit()
  {
    // this.total = this.expenseService.getAllExpensesForMonth(this.expenseService.
    // monthlyReportForm.get('fromDate').value, this.expenseService.monthlyReportForm.get('toDate').value, this.expenseList, this.total);
  }


  openCreateExpenseModal() {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.id = "modal-component";
    dialogConfig.height = "700px";
    dialogConfig.width = "600px";

    const modalDialog = this.matDialog.open(CreateExpenseFormComponent, dialogConfig);

  }



  deleteExpenseModal(data: Expense) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.id = "modal-component";
    dialogConfig.height = "200px";
    dialogConfig.width = "300px";
    dialogConfig.data = {expense: data};
    const modalDialog = this.matDialog.open(DeleteExpenseComponent, dialogConfig);

  }

  openUpdateExpenseModal(data: Expense) {
    console.log('method call openUpdateExpenseModal');
    const dialogConfig = new MatDialogConfig();
    this.expenseService.populateCreateExpenseFormData(data);

    dialogConfig.disableClose = true;
    dialogConfig.id = "modal-component";
    dialogConfig.height = "700px";
    dialogConfig.width = "600px";
    dialogConfig.data = {expense: data};

    const modalDialog = this.matDialog.open(CreateExpenseFormComponent, dialogConfig);

  }

  openDeleteExpenseModal(data: Expense) {
    console.log('method call openUpdateExpenseModal');
    const dialogConfig = new MatDialogConfig();
   // this.expenseService.populateCreateExpenseFormData(data);

    dialogConfig.disableClose = true;
    dialogConfig.id = "modal-component";
    dialogConfig.height = "200px";
    dialogConfig.width = "500px";
    dialogConfig.data = {expense: data};

    const modalDialog = this.matDialog.open(DeleteExpenseComponent, dialogConfig);

  }


  getAllExpenses(){

    this.expenseService.getAllExpensesNew().subscribe((data: {}) => {

       this.expenseList.data = data; // on data receive populate dataSource.data array
       this.expenseList.sort = this.sort;
       this.expenseList.paginator = this.paginator;
    });

  }

  test(){
    this.expenseList =[new Expense(1,'tetst',null,null,null,null)];

  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.expenseList.filter = filterValue.trim().toLowerCase();
  }


  editExpense(expense: Expense)
  {
    console.log('inside Edit Expense');

  }


}
