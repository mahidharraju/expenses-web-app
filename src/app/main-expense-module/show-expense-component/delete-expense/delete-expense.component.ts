import { ExpenseService } from './../../services/expense-service';
import { Component, Inject, Optional, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Expense } from '../../models/expense';


@Component({
  selector: 'app-delete-expense',
  templateUrl: './delete-expense.component.html',
  styleUrls: ['./delete-expense.component.styl']
})
export class DeleteExpenseComponent implements OnInit {

  expense: Expense;


  constructor(private expenseService: ExpenseService, public dialogRef: MatDialogRef<DeleteExpenseComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any
    ) {
      this.expense = data.expense;
    }


  fromPage: string;

  ngOnInit(): void {
  }

  deletExpese() {

    this.expenseService.deleteExpense(this.expense);

    this.closeModal();
  }

  // If the user clicks the cancel button a.k.a. the go back button, then\
  // just close the modal
  closeModal() {
    console.log('hi');
    this.dialogRef.close();
  }

}
