import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowExpensesByRangeComponent } from './show-expenses-by-range.component';

describe('ShowExpensesByRangeComponent', () => {
  let component: ShowExpensesByRangeComponent;
  let fixture: ComponentFixture<ShowExpensesByRangeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowExpensesByRangeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowExpensesByRangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
