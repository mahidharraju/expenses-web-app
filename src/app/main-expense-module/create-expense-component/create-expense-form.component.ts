import { ContributorService } from './../services/contributor-service';
import { Component, OnInit, Inject, Optional, } from '@angular/core';
import { ExpenseService } from '../services/expense-service';


@Component({
  selector: 'app-create-expense-form',
  templateUrl: './create-expense-form.component.html',
  styleUrls: ['./create-expense-form.component.styl']
})
export class CreateExpenseFormComponent implements OnInit {

  expense: any;

  constructor(
                public expenseService: ExpenseService){
             }

  submit(){
            this.expenseService.createExpense(this.expenseService.createExpenseForm.value);
           }

  ngOnInit(): void {
  }
}
