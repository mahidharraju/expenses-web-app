import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuComponentComponent } from './menu-component/menu-component.component';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';
import { AppRoutingModule } from '../app-routing.module';
import { HeaderComponentComponent } from './header-component/header-component.component';
import { FooterComponentComponent } from './footer-component/footer-component.component';
import { CreateExpenseFormComponent } from './create-expense-component/create-expense-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { CategoryComponent } from './category-component/category-component';
import { CreateCategoryComponent } from './category-component/create-category/create-category.component';
import { ShowExpenseComponent } from './show-expense-component/show-expense.component';
import { MonthlyExpenseComponent } from './show-expense-component/monthly-expense-component/monthly-expense.component';
import { DailyExpenseComponent } from './show-expense-component/daily-expense-component/daily-expense.component';
import { ContributorComponent } from './contributor-component/contributor.component';
import { ShowExpensesByRangeComponent } from './show-expense-component/show-expenses-by-range/show-expenses-by-range.component';
import { UpdateCategoryComponent } from './category-component/update-category/update-category.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTableModule } from '@angular/material/table';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';
import { DeleteExpenseComponent } from './show-expense-component/delete-expense/delete-expense.component';



const routes: Routes = [


  { path: 'createExpenseForm', component: CreateExpenseFormComponent,  outlet: 'mainmenu'},

  { path: 'displayExpenses', component: MonthlyExpenseComponent,  outlet: 'mainmenu',
    children: [
      { path: 'expensesByRange', component: ShowExpensesByRangeComponent, outlet: 'submenu'},
      { path: 'dailyExpenseReport', component: DailyExpenseComponent, outlet: 'submenu'}
    ]
  },



  { path: 'category', component: CategoryComponent, outlet: 'mainmenu',
    children: [
      { path: 'createCategory', component: CreateCategoryComponent,  outlet: 'submenu'},
      { path: 'updateCategory', component: UpdateCategoryComponent, outlet: 'submenu'},
      { path: 'deleteCategory', component: CreateCategoryComponent, outlet: 'submenu'}
    ]
  },



  { path: 'contributor', component: ContributorComponent, outlet: 'mainmenu'}


];


@NgModule({
  declarations: [MenuComponentComponent, HeaderComponentComponent, FooterComponentComponent, CreateExpenseFormComponent,
    CategoryComponent,
    CreateCategoryComponent,
    ShowExpenseComponent,
    MonthlyExpenseComponent,
    DailyExpenseComponent,
    ContributorComponent,
    ShowExpensesByRangeComponent,
    UpdateCategoryComponent,
    DeleteExpenseComponent],
  imports: [
    CommonModule,
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule.forRoot(routes),
    HttpClientModule,
    NoopAnimationsModule,
    MatButtonModule,
    MatDialogModule,
    MatTableModule,
    MatFormFieldModule,
    MatIconModule,
    MatSortModule,
    MatPaginatorModule
  ],
  exports: [RouterModule],
  bootstrap:    [ MenuComponentComponent ],
  entryComponents: [CreateExpenseFormComponent]
})




export class MainExpenseModuleModule { }
